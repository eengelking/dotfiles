#!/usr/bin/env bash

###################
## Dotfiles Installer
## This script will install the necessary software and configure Dotfiles for Ubuntu or macOS.
## 2020-2024 (c) Ed Engelking
## License: MIT
###################


## =============================================================================
## Variables
## =============================================================================
ZSH_CUSTOM="${HOME}/.oh-my-zsh/custom"
USERNAME=`whoami`
DIR_ROOT="$1"
GREEN='\033[0;32m'
CYAN='\033[1;36m'
ORANGE='\033[0;33m'
RED='\033[0;31m'
CLEAR='\033[0m'


## =============================================================================
## Cleanly exit the script if CTRL+C is pressed
## =============================================================================
trap ctrl_c INT
function ctrl_c() {
  echo -e "${RED}\n-----\nExiting.${CLEAR}"
  exit 1
}


## =============================================================================
## Only run this script if the path is set
## =============================================================================
if [[ -z "$1" ]]; then
  echo -e "${ORANGE}Do not run this script directly. Use the top level install.sh command instead.${CLEAR}"
  exit 0
fi


## =============================================================================
## Verify the directory path
## =============================================================================
if [[ ! -f "${DIR_ROOT}/os/ubuntu/install.sh" ]]; then
  echo -e "${ORANGE}\nThe directory path is invalid:${CLEAR}"
  echo "${DIR_ROOT}/os/ubuntu/install.sh"
  exit 1
fi


## =============================================================================
## Load the .env file
## =============================================================================
if [[ ! -s ${DIR_ROOT}/.env ]]; then
  echo -e "${ORANGE}\nThe .env file is empty:${CLEAR}"
  echo "${DIR_ROOT}/.env"
    exit 1
fi
source ${DIR_ROOT}/.env


## =============================================================================
## Copy dotfiles
## =============================================================================
echo -e "${GREEN}\n-----\nCopying dotfiles.\n-----${CLEAR}"
sleep 1
sudo cp ${DIR_ROOT}/dotfiles/aliases ${HOME}/.aliases
sudo chown ${USERNAME}:${USERNAME} ${HOME}/.aliases
sudo cp ${DIR_ROOT}/dotfiles/gitconfig ${HOME}/.gitconfig
sudo chown ${USERNAME}:${USERNAME} ${HOME}/.gitconfig
sudo cp ${DIR_ROOT}/dotfiles/vimrc ${HOME}/.vimrc
sudo chown ${USERNAME}:${USERNAME} ${HOME}/.vimrc
sudo cp ${DIR_ROOT}/dotfiles/zshrc ${HOME}/.zshrc
sudo chown ${USERNAME}:${USERNAME} ${HOME}/.zshrc
echo -e "${CYAN}Complete!${CLEAR}"


## =============================================================================
## Dotfiles Complete. 
## =============================================================================
echo -e "${GREEN}\n-----\nDotfiles Installation Finished!\n-----${CLEAR}"