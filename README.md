# Ed's Default Dotfiles
This repository contains the dotfiles I use for Ubuntu and macOS. It is designed to be used with a fresh installation of Ubuntu or macOS, or it can just update the CLI and copy the dotfiles alone.

## Requirements
* Git
* VIM, nano, or other text editor
* Internet access

## .env File
Please review and edit the `.env` to set the required variables prior to running the script. The installer will not allow you to run until the `.env` file has been modified.

## Installation
Clone the repo and run the install script:
```bash
git clone https://gitlab.com/eengelking/dotfiles.git /tmp/dotfiles
/tmp/dotfiles/install.sh
```

When prompted, select an option:
```bash
-----
What do you want to do?
-----
1) Ubuntu Configuration
2) macOS Configuration
3) Oh-My-ZSH Configuration
4) Dotfiles Configuration
5) Exit
```

### CLI Arguments
It is also possible to run the command line installer to bypass the prompt. The arguments are as follows:

* ubuntu
* macos
* zsh
* dotfiles
* -h or --help for the help menu

```bash
## Example
/tmp/dotfiles/install.sh ubuntu
```