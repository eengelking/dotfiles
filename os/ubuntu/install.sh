#!/usr/bin/env bash

###################
## Ubuntu Installer
## This script will install the necessary software and configure the system for Ubuntu.
## 2020-2024 (c) Ed Engelking
## License: MIT
###################

## =============================================================================
## Variables, DO NOT CHANGE
## =============================================================================
DIR_ROOT="$1"
GREEN='\033[0;32m'
CYAN='\033[1;36m'
ORANGE='\033[0;33m'
RED='\033[0;31m'
CLEAR='\033[0m'


## =============================================================================
## Cleanly exit the script if CTRL+C is pressed
## =============================================================================
trap ctrl_c INT
function ctrl_c() {
  echo -e "${RED}\n-----\nExiting.${CLEAR}"
  exit 1
}


## =============================================================================
## Only run this script if the path is set
## =============================================================================
if [[ -z "$1" ]]; then
  echo -e "${ORANGE}Do not run this script directly. Use the top level install.sh command instead.${CLEAR}"
  exit 0
fi


## =============================================================================
## Verify the directory path
## =============================================================================
if [[ ! -f "${DIR_ROOT}/os/ubuntu/install.sh" ]]; then
  echo -e "${ORANGE}\nThe directory path is invalid:${CLEAR}"
  echo "${DIR_ROOT}/os/ubuntu/install.sh"
  exit 1
fi


## =============================================================================
## Load the .env file
## =============================================================================
if [[ ! -s ${DIR_ROOT}/.env ]]; then
  echo -e "${ORANGE}\nThe .env file is empty:${CLEAR}"
  echo "${DIR_ROOT}/.env"
    exit 1
fi
source ${DIR_ROOT}/.env


## =============================================================================
## Configure the Node
## =============================================================================
echo -e "${GREEN}\n-----\nConfiguring the Node.\n-----${CLEAR}"
sleep 1
sudo timedatectl set-timezone ${TIMEZONE}
sudo hostnamectl set-hostname ${HOSTNAME}
echo "fs.file-max = ${FS_FILE_MAX}" | sudo tee /etc/sysctl.d/99-file-max.conf 2>/dev/null >/dev/null
echo -e "${CYAN}Complete!${CLEAR}"


## =============================================================================
## Software Installation
## =============================================================================

##########
## Install Docker
##########
echo -e "${GREEN}\n-----\nUpdating APT and upgrading existing packages.\n-----${CLEAR}"
sleep 1
## Update APT and update existing packages
sudo apt-get -qq update 2>/dev/null >/dev/null && sudo apt-get -qq upgrade -y 2>/dev/null >/dev/null
echo -e "${CYAN}Complete!${CLEAR}"
## Configure Docker for installation
echo -e "${GREEN}\n-----\nConfiguring Docker for installation.\n-----${CLEAR}"
sleep 1
## Uninstall existing Docker packages
DEBIAN_FRONTEND=noninteractive sudo apt-get remove docker docker-engine docker.io containerd runc -y 2>/dev/null >/dev/null
## Install required packages for Docker
DEBIAN_FRONTEND=noninteractive sudo apt-get install -y ca-certificates curl gnupg 2>/dev/null >/dev/null
## Add the Docker GPG key
sudo install -m 0755 -d /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
sudo chmod a+r /etc/apt/keyrings/docker.gpg
echo \
  "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
## Add the Docker group
sudo groupadd docker
echo -e "${CYAN}Complete!${CLEAR}"

##########
## Install APT packages
##########
echo -e "${GREEN}\n-----\nInstalling Packages via APT. This may take a few minutes.\n-----${CLEAR}"
sleep 1
sudo apt-get -qq update 2>/dev/null >/dev/null
DEBIAN_FRONTEND=noninteractive xargs sudo apt-get -qq install -y < ${DIR_ROOT}/os/ubuntu/Aptfile 2>/dev/null >/dev/null
jq --version
echo -e "${CYAN}Complete!${CLEAR}"

##########
## Install SNAP packages
##########
echo -e "${GREEN}\n-----\nInstalling Packages via SNAP. This may take a few minutes.\n-----${CLEAR}"
sleep 1
sudo snap install multipass 2>/dev/null >/dev/null
sudo snap refresh
multipass version
echo -e "${CYAN}Complete!${CLEAR}"

##########
## Install Homebrew for Linux
##########
echo -e "${GREEN}\n-----\nInstalling Homebrew for Linux. This may take a few minutes.\n-----${CLEAR}"
sleep 1
NONINTERACTIVE=1 /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)" 2>/dev/null >/dev/null
/home/linuxbrew/.linuxbrew/bin/brew --version
echo -e "${CYAN}Complete!${CLEAR}"

##########
## Install the Homebrew packages
##########
echo -e "${GREEN}\n-----\nInstalling the Homebrew packages. This may take a few minutes.\n-----${CLEAR}"
sleep 1
/home/linuxbrew/.linuxbrew/bin/brew bundle --file=${DIR_ROOT}/os/ubuntu/Brewfile 2>/dev/null >/dev/null
/home/linuxbrew/.linuxbrew/bin/brew list

##########
## Install Golang
##########
echo -e "${GREEN}\n-----\nInstalling Golang v${VERSION_GO}.\n-----${CLEAR}"
sleep 1
curl -fsSL https://golang.org/dl/go${VERSION_GO}.linux-${OS_ARCH}.tar.gz | sudo tar -C /usr/local -xzf -
/usr/local/go/bin/go version
echo -e "${CYAN}Complete!${CLEAR}"

##########
## Install Node Version Manager (NVM)
##########
echo -e "${GREEN}\n-----\nInstalling Node Version Manager v${VERSION_NVM}.\n-----${CLEAR}"
sleep 1
curl -fsSL https://raw.githubusercontent.com/nvm-sh/nvm/v${VERSION_NVM}/install.sh | bash 2>/dev/null >/dev/null
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
nvm --version
echo -e "${CYAN}Complete!${CLEAR}"

##########
## Install Node
##########
echo -e "${GREEN}\n-----\nInstalling Node v${VERSION_NODE}.\n-----${CLEAR}"
sleep 1
nvm install ${VERSION_NODE} 2>/dev/null >/dev/null
node --version
echo -e "${CYAN}Complete!${CLEAR}"


## =============================================================================
## User Setup
## =============================================================================
echo -e "${GREEN}\n-----\nAdding the ${USERNAME} user.\n-----${CLEAR}"
sleep 1

## Check to see if the user exists. If not, create the user.
if ! id -u ${USERNAME} &>/dev/null; then
  sudo useradd -m -s /bin/bash ${USERNAME}
fi
## Add the user to the necessary groups
sudo usermod -aG docker,sudo,${USERNAME} ${USERNAME}
## Create the git directory and set the permissions
sudo mkdir -p /home/${USERNAME}/git
sudo chown -R ${USERNAME}:${USERNAME} /home/${USERNAME}/git
## Create the SSH directory and set the permissions
sudo mkdir -p /home/${USERNAME}/.ssh
sudo touch /home/${USERNAME}/.ssh/authorized_keys
echo "${SSH_PUBKEY}" | sudo tee /home/${USERNAME}/.ssh/authorized_keys 2>/dev/null >/dev/null
sudo chown -R ${USERNAME}:${USERNAME} /home/${USERNAME}/.ssh
sudo chmod 700 /home/${USERNAME}/.ssh
sudo chmod 600 /home/${USERNAME}/.ssh/authorized_keys
## Update the sudoers.d file with the user's permissions
echo "${USERNAME} ${SUDO}" | sudo tee /etc/sudoers.d/${USERNAME} 2>/dev/null >/dev/null
## Set the user's shell to zsh
sudo chsh -s /bin/zsh ${USERNAME}
## Complete message
echo -e "${CYAN}Complete!${CLEAR}"


## =============================================================================
## Set permissions for the dotfiles for the user
## =============================================================================
echo -e "${GREEN}\n-----\nSetting Permissions for the Dotfiles.\n-----${CLEAR}"
sleep 1
sudo chown -R ${USERNAME}:${USERNAME} ${DIR_ROOT}
echo -e "${CYAN}Complete!${CLEAR}"


## =============================================================================
## Ubuntu Complete. 
## =============================================================================
echo -e "${GREEN}\n-----\nUbuntu Installation Finished!\n-----${CLEAR}"


## =============================================================================
## If the FULL_INSTALL variable is set to true, continue with the CLI installation.
## =============================================================================
if [[ $FULL_INSTALL == "true" ]]; then
  echo -e "${ORANGE}\nFull install. Continuing with ZSH Configuration Installation.${CLEAR}"
  sleep 1
  ${DIR_ROOT}/zsh/install.sh ${DIR_ROOT}
fi