#!/usr/bin/env bash

###################
## Oh-My-ZSH Installer
## This script will install the necessary software and configure Oh-My-ZSH for Ubuntu or macOS.
## 2020-2024 (c) Ed Engelking
## License: MIT
###################


## =============================================================================
## Variables
## =============================================================================
ZSH_CUSTOM="${HOME}/.oh-my-zsh/custom"
USERNAME=`whoami`
DIR_ROOT="$1"
GREEN='\033[0;32m'
CYAN='\033[1;36m'
ORANGE='\033[0;33m'
RED='\033[0;31m'
CLEAR='\033[0m'


## =============================================================================
## Cleanly exit the script if CTRL+C is pressed
## =============================================================================
trap ctrl_c INT
function ctrl_c() {
  echo -e "${RED}\n-----\nExiting.${CLEAR}"
  exit 1
}


## =============================================================================
## Only run this script if the path is set
## =============================================================================
if [[ -z "$1" ]]; then
  echo -e "${ORANGE}Do not run this script directly. Use the top level install.sh command instead.${CLEAR}"
  exit 0
fi


## =============================================================================
## Verify the directory path
## =============================================================================
if [[ ! -f "${DIR_ROOT}/os/ubuntu/install.sh" ]]; then
  echo -e "${ORANGE}\nThe directory path is invalid:${CLEAR}"
  echo "${DIR_ROOT}/os/ubuntu/install.sh"
  exit 1
fi


## =============================================================================
## Load the .env file
## =============================================================================
if [[ ! -s ${DIR_ROOT}/.env ]]; then
  echo -e "${ORANGE}\nThe .env file is empty:${CLEAR}"
  echo "${DIR_ROOT}/.env"
    exit 1
fi
source ${DIR_ROOT}/.env


## =============================================================================
## Clean-up existing OMZ install
## =============================================================================
echo -e "${GREEN}\n-----\nCleaning up existing Oh My Zsh install.\n-----${CLEAR}"
sleep 1

## Determine if OMZ is installed
if [[ -d "${HOME}/.oh-my-zsh" ]]; then
  echo -e "${CYAN}Oh My Zsh is installed. Removing it.${CLEAR}"
  rm -Rf "${HOME}/.oh-my-zsh"
fi
echo -e "${CYAN}Complete!${CLEAR}"


## =============================================================================
## Download OMZ installer, set to execute, and install.
## =============================================================================
echo -e "${GREEN}\n-----\nDownloading and installing Oh My Zsh.\n-----${CLEAR}"
sleep 1
curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh -o /tmp/omz.sh
chmod +x /tmp/omz.sh
sh /tmp/omz.sh --unattended 2>/dev/null >/dev/null
echo -e "${CYAN}Complete!${CLEAR}"


## =============================================================================
## Clone plugins for Oh-My-Zsh
## autosuggestions, history search, syntax highlighting, spaceship theme.
## =============================================================================
echo -e "${GREEN}\n-----\nCloning plugins.\n-----${CLEAR}"
sleep 1
git clone "https://github.com/zsh-users/zsh-autosuggestions" "${ZSH_CUSTOM}/plugins/zsh-autosuggestions" 2>/dev/null >/dev/null
git clone "https://github.com/zsh-users/zsh-history-substring-search" "${ZSH_CUSTOM}/plugins/zsh-history-substring-search" 2>/dev/null >/dev/null
git clone "https://github.com/zsh-users/zsh-syntax-highlighting.git" "${ZSH_CUSTOM}/plugins/zsh-syntax-highlighting" 2>/dev/null >/dev/null
git clone "https://github.com/denysdovhan/spaceship-prompt.git" "${ZSH_CUSTOM}/themes/spaceship-prompt" --depth=1 2>/dev/null >/dev/null
ln -s "${ZSH_CUSTOM}/themes/spaceship-prompt/spaceship.zsh-theme" "${ZSH_CUSTOM}/themes/spaceship.zsh-theme" 2>/dev/null >/dev/null
echo -e "${CYAN}Complete!${CLEAR}"


## =============================================================================
## Cleanup
## =============================================================================
echo -e "${GREEN}\n-----\nCleaning up.\n-----${CLEAR}"
sleep 1
rm /tmp/omz.sh
echo -e "${CYAN}Complete!${CLEAR}"


## =============================================================================
## Oh-My-ZSH Complete. 
## =============================================================================
echo -e "${GREEN}\n-----\nOh-My-ZSH Installation Finished!\n-----${CLEAR}"


## =============================================================================
## If the FULL_INSTALL variable is set to true, continue with the CLI installation.
## =============================================================================
if [[ $FULL_INSTALL == "true" ]]; then
  echo -e "${ORANGE}\nFull install. Continuing with Dotfiles Configuration Installation.${CLEAR}"
  sleep 1
  ${DIR_ROOT}/dotfiles/install.sh ${DIR_ROOT}
fi