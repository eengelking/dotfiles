#!/usr/bin/env bash

###################
## Launching Installer
## This script will install the necessary software and configure the system for Ubuntu or macOS.
## 2020-2024 (c) Ed Engelking
## License: MIT
###################


## =============================================================================
## Variables
## =============================================================================
GREEN='\033[0;32m'
CYAN='\033[1;36m'
ORANGE='\033[0;33m'
RED='\033[0;31m'
CLEAR='\033[0m'
DIR_ROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"


## =============================================================================
## Cleanly exit the script if CTRL+C is pressed
## =============================================================================
trap ctrl_c INT
function ctrl_c() {
  echo -e "${RED}\n-----\nExiting.${CLEAR}"
  exit 1
}


## =============================================================================
## Help function
## =============================================================================
help() {
  echo -e "${CYAN}\nUsage:\t\t./install.sh [OPTION]${CLEAR}"
  echo -e "${CYAN}Example:\t./install.sh ubuntu${CLEAR}"
  echo -e "${CYAN}\nOptions:\n${CLEAR}"
  echo -e "${CYAN}-h, --help${CLEAR}\tDisplay this help menu."
  echo -e "${CYAN}ubuntu${CLEAR}\t\tDeploy Ubuntu configuration."
  echo -e "${CYAN}macos${CLEAR}\t\tDeploy macOS configuration."
  echo -e "${CYAN}zsh${CLEAR}\t\tDeploy Oh-My-ZSH configuration."
  echo -e "${CYAN}dotfiles${CLEAR}\t\tDeploy Dotfiles configuration."
  echo -e "\nIf no option is specified, the script will prompt you to choose an option."
  exit 0
}


## =============================================================================
## Check the .env file to see if the CONFIGURED variable is set to true.
## =============================================================================
configured() {
  if [[ -f ${DIR_ROOT}/.env ]]; then
    source ${DIR_ROOT}/.env
    if [[ $CONFIGURED != "true" ]]; then
      echo -e "${RED}\n-----\nThe .env file has not yet been configured. Please edit the file before continuing.\n-----${CLEAR}"
      exit 0
    fi
  fi
}


## =============================================================================
## Check for the .env file
## =============================================================================
if [[ ! -f ${DIR_ROOT}/.env ]]; then
  echo -e "${RED}\n-----\nNo .env file found. Please create one before continuing.\n-----${CLEAR}"
  exit 0
fi


## =============================================================================
## Check for the CONFIGURED variable in the .env file
## =============================================================================
configured


## =============================================================================
## Validate the user's choice
## =============================================================================
if [[ $1 == "ubuntu" ]]; then
  CHOICE=1
elif [[ $1 == "macos" ]]; then
  CHOICE=2
elif [[ $1 == "zsh" ]]; then
  CHOICE=3
elif [[ $1 == "dotfiles" ]]; then
  CHOICE=4
elif [[ $1 == "-h" ]] || [[ $1 == "--help" ]]; then
  help
elif [[ -z "$1" ]]; then
  CHOICE=0
else
  help
fi


## =============================================================================
## Prompt the user if no option is specified
## =============================================================================
if [[ $CHOICE = 0 ]]; then
  # Ask the user what they want to do: os or cli
  echo -e "${GREEN}\n-----\nWhat do you want to do?\n-----${CLEAR}"
  echo -e "${CYAN}1)${CLEAR} Ubuntu Configuration"
  echo -e "${CYAN}2)${CLEAR} macOS Configuration"
  echo -e "${CYAN}3)${CLEAR} Oh-My-ZSH Configuration"
  echo -e "${CYAN}4)${CLEAR} Dotfiles Configuration"
  echo -e "${CYAN}5)${CLEAR} Exit"
  read -p "Enter your choice: " CHOICE
fi


## =============================================================================
## Take the result of the user's choice and run the appropriate script.
## =============================================================================
case $CHOICE in
  1)
    echo -e "${ORANGE}\nRunning the Ubuntu Configuration Installer.\n${CLEAR}"
    sleep 1
    ${DIR_ROOT}/os/ubuntu/install.sh ${DIR_ROOT}
    ;;
  2)
    echo -e "${ORANGE}\nRunning the macOS Configuration Installer.\n${CLEAR}"
    sleep 1
    echo -e "${RED}This is not yet implemented.${CLEAR}"
    ;;
  3)
    echo -e "${ORANGE}\nRunning the Oh-My-ZSH Configuration Installer.\n${CLEAR}"
    sleep 1
    ${DIR_ROOT}/zsh/install.sh ${DIR_ROOT}
    ;;
  4) 
    echo -e "${ORANGE}\nRunning the Dotfiles Configuration Installer.\n${CLEAR}"
    sleep 1
    ${DIR_ROOT}/dotfiles/install.sh ${DIR_ROOT}
    ;;
  5)
    echo -e "${ORANGE}\n-----\nExiting.\n-----${CLEAR}"
    exit 0
    ;;
  *)
    echo -e "${RED}\n-----\nInvalid choice. Exiting.\n-----${CLEAR}"
    exit 1
    ;;
esac


## =============================================================================
## Installation Complete. End of Script.
## =============================================================================
echo -e "${ORANGE}\n-----\nInstallation Complete.\n-----${CLEAR}"
echo -e "${CYAN}-->  Please restart your terminal session.  <--\n\n${CLEAR}"
exit 0